# MathALÉA

Parce que [Anki](https://apps.ankiweb.net/) peut aussi intégrer des exercices de [MathALÉA](https://coopmaths.fr/alea/) !

## Guide d'utilisation

### Version texte

1. Télécharger le paquet [MathALEA.apkg](https://forge.apps.education.fr/anki-templates/mathalea/-/raw/master/MathALEA.apkg)
2. L'ouvrir avec Anki
3. Le type de note MathALEA a été ajouté ainsi qu'un paquet éponyme. Vous pouvez supprimer le paquet sans problème, le type de note ne sera pas supprimé (et c'est lui qui nous intéresse)
4. Ajouter une carte
    1. Choisir (en haut à gauche) le type de note MathALEA
    2. Vérifier (en haut à droite) le paquet dans lequel on va créer la carte
    3. Coller un lien vers un exercice de MathALÉA dans le champ URL
    4. Compléter le champ Titre avec ce que vous voulez (c'est sous ce nom que la carte apparaîtra dans le gestionnaire de cartes, il doit être unique)
5. Répéter l'étape 4 à volonté !

### Version vidéo

<a href="https://tube-numerique-educatif.apps.education.fr/w/a4yMbcXVpZJN4vCHigsUBG"><img src="images/vignette.png" height="200px" /></a>

### Problème sur Android

Sur Android, il arrive souvent que, pour une raison que j'ignore, le paquet MathALEA.apkg se renomme tout seul MathALEA.apkg.zip après téléchargement et l'ouverture avec AnkiDroid ne fonctionne pas. Une solution consiste alors en :

1. Renommer le fichier en MathALEA.apkg (sans le .zip)
2. Ouvrir AnkiDroid
3. Cliquer sur les trois points en haut à droite puis sur "Importer"
4. Retrouver le fichier MathALEA.apkg pour l'importer (regarder dans les fichiers récents)

### Problème sur iOS

Sur iOS, l'application fonctionne bien mais est payante, on peut cependant utiliser gratuitement la version web : [AnkiWeb](https://ankiweb.net).

L'une des limitations de cette version est qu'on ne peut pas importer de paquets. Le moyen de contourner cela consiste à installer Anki sur un ordinateur et à synchroniser son téléphone avec son ordinateur comme expliqué dans ce tutoriel par exemple : [Tutoriel d'installation de Anki](https://topmaths.fr/?v=tutos&ref=installation-anki)

La bonne nouvelle c'est qu'on n'a besoin d'un ordinateur que la première fois pour importer le paquet, après on peut ajouter des nouvelles cartes directement depuis le téléphone !

## Captures d'écran

### Desktop

|Recto|Verso|
|:---:|:---:|
|![](/images/desktop-recto.png)|![](/images/desktop-verso.png)|

### AnkiMobile

|Recto|Verso|
|:---:|:---:|
|![](/images/ankimobile-recto.png)|![](/images/ankimobile-verso.png)|
